from numpy import pi, power

k    = 1.380649e-16 #erg/K
hbar = 1.05457266e-27 # erg*s
h    = 6.626070040e-27#erg*s 

m_H = 1.6735575e-24 # g

mu_B = 927.400915e-23 # эрг/Гс;
Radius = 5.5 # cm
Hight = 15 #cm
RadiusT2 = 5.5 # cm
HightT2 = 15 #cm

Ea = 1.14 #K
a = 0.72*1e-8 #cm 

sigma = 8*pi*power(a, 2)

G_rel = 1e-15

H_0 = 1# Tesla
initial_Delta = 7.0/6.0 #Turku trap
initialN = 1e14

GlobalLenght = 50