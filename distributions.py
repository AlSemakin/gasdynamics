#distributions

def F_T(e, T):
	K1 = 8*pi*e/(m_H)
	K2 = power(m_H/(2*pi*k*T), 1.5)
	return K1*K2*exp(-e/(T))

def F_v(v, T):
	K1 = 4*pi*power(v, 2)
	K2 = power(m_H/(2*pi*k*T), 1.5)
	return K1*K2*exp(-m_H * power(v,2)/(2*k*T))

def norm_F_T(_T, _lenght):
	locl_e = np.linspace(1e-6, 1, _lenght)
	summ = 0.0
	for _e in locl_e:
		summ += F_T(_e, _T)
	return summ

def PartOfTrappedAtoms(_T, _e_w):
	e_distr = np.linspace(0, 1, 1000)
	summ = 0.0
	for _e in e_distr:
		if _e < _e_w:
			umm += F_T(_e, _T)

	res = summ/norm_F_T(_T, 1000)
	return res



def n0_inf(_T        :float,
           _N        :float,
           _epsilon_w:float, 
           _epsilon_z:float):
  '''
  Density with out the trunction
                  N
  n0 = ------------------------
       Lambda(T)^3 * zeta_inf(T)
  '''
  return _N/(power(Lambda(_T), 3)* zeta_inf(_T, _epsilon_w, _epsilon_z))

def n0(_T        :float,
       _epsilon_t:float,
       _N        :float,
       _epsilon_w:float, 
       _epsilon_z:float):
  '''
  Density with the trunction
                  N
  n0 = ------------------------
       Lambda(T)^3 * zeta(T, e_t)
  '''
  return _N/(power(Lambda(_T), 3)* zeta(_T, _epsilon_t, _epsilon_w, _epsilon_z))


def n(_radius   :float,
      _z        :float,
      _T        :float,
      _epsilon_t:float,
      _N        :float,
      _epsilon_w:float, 
      _epsilon_z:float):
  '''
  Density distribution(with the trunction)

  n(r,z,T,e_t) = n0(T,e_t)*exp(-U(r,z)/k*T)*P(3/2, eta),
  _r - coodr along r-axis
  _z - coodr along z-axis
  _T - quasi temperature
  _epsilon_t - trunction
  '''

  _eta = (_epsilon_t - potU(_radius, _z))/(k*_T)

  # return n0(_T, _epsilon_t)*exp(-potU(_radius, _z)/(k*_T))*gammainc(1.5, _eta)
  return n_inf(_radius,_z,_T,_epsilon_t, _N, _epsilon_w, _epsilon_z)*gammainc(1.5, _eta)

def n_inf(_radius   :float,
          _z        :float,
          _T        :float,
          _espilon_t:float,
          _Number   :float,
          _epsilon_w:float, 
          _epsilon_z:float):
  '''
  Density distribution(with out the trunction)

  n_inf(r,z,T) = n0_inf(T)*exp(-U(r,z)/k*T),
  _r - coodr along r-axis
  _z - coodr along z-axis
  _T - quasi temperature
  '''
  return n0(_T,_espilon_t,_Number,_epsilon_w, _epsilon_z)*exp(-potU(_radius, _z)/(k*_T))


def f_tr(_epsilon  :float, 
         _T        :float, 
         _epsilon_t:float,
         _N        :float,
         _epsilon_w:float,
         _epsilon_z:float):
  '''
  The enery distribution of the non-degenerative regime
  '''
  if _epsilon < _epsilon_t:
    return Z(_T, _epsilon_t, _N, _epsilon_w, _epsilon_z)*np.exp(-_epsilon/(k*_T))
  else:
    return 0.0


def f_tr_T2(_epsilon  :float, 
            _T        :float, 
            _epsilon_t:float,
            _N        :float,
            _epsilon_w:float,
            _epsilon_z:float,
            _n        :float):
  '''
  The enery distribution of the non-degenerative regime
  '''
  if _epsilon < _epsilon_t:
    return Z_T2(_T, _epsilon_t, _N, _epsilon_w,_epsilon_z,_n)*exp(-_epsilon/(k*_T))
  else:
    return 0.0

def n0_inf_T2(_T        :float,
              _N        :float,
              _epsilon_w:float, 
              _epsilon_z:float,
              _n        :float,
               u0       :float = 0):
  '''
  Density with out the trunction
                  N
  n0 = ------------------------
       Lambda(T)^3 * zeta_inf(T)
  '''
  return _N/(power(Lambda(_T), 3)* zeta_inf_T2(_T,_epsilon_w,_epsilon_z,_n, u0))

def n0_T2(_T        :float,
          _epsilon_t:float,
          _N        :float,
          _epsilon_w:float, 
          _epsilon_z:float,
          _n        :float,
          _u0       :float):
  '''
  Density with the trunction
                  N
  n0 = ------------------------
       Lambda(T)^3 * zeta(T, e_t)
  '''
  return _N/(power(Lambda(_T), 3)* zeta_T2(_T, _epsilon_t, _epsilon_w, _epsilon_z, _n, _u0))

def n_T2(_radius   :float,
         _z        :float,
         _T        :float,
         _epsilon_t:float,
         _N        :float,
         _epsilon_w:float, 
         _epsilon_z:float,
         _n        :float,
         _u0       :float):
  '''
  Density distribution(with the trunction)

  n(r,z,T,e_t) = n0(T,e_t)*exp(-U(r,z)/k*T)*P(3/2, eta),
  _r - coodr along r-axis
  _z - coodr along z-axis
  _T - quasi temperature
  _epsilon_t - trunction
  '''

  _eta = (_epsilon_t - potU_T2(_radius, _z, _epsilon_w, _epsilon_z, _n, _u0))/(k*_T)

  # return n0(_T, _epsilon_t)*exp(-potU(_radius, _z)/(k*_T))*gammainc(1.5, _eta)
  return n0_inf_T2(_radius,_z,_T,_epsilon_t, _N, _epsilon_w, _epsilon_z,_n, _u0)*gammainc(1.5, _eta)

def n_inf_T2(_radius   :float,
             _z        :float,
             _T        :float,
             _espilon_t:float,
             _Number   :float,
             _epsilon_w:float, 
             _epsilon_z:float,
             _n        :float,
             _u0       :float = 0):
  '''
  Density distribution(with out the trunction)

  n_inf(r,z,T) = n0_inf(T)*exp(-U(r,z)/k*T),
  _r - coodr along r-axis
  _z - coodr along z-axis
  _T - quasi temperature
  '''
  return n0_T2(_T,_espilon_t,_Number, _epsilon_w, _epsilon_z,_n, u0)*exp(-potU_T2(_radius, _z, _epsilon_w, _epsilon_z, _n, _u0)/(k*_T))



def F_max(v,T):
	K = power(m_H/(2*pi*k*T),1.5)
	K1 = exp(-m_H*v**2/(2*k*T))
	K2 = 4*pi*v**2
	return K*K1*K2

def F_max_T(t,T):
	# v = sqrt(2*k*t/m_H)
	v = sqrt(3*k*t/m_H)
	K = power(m_H/(2*pi*k*T),1.5)
	K1 = exp(-m_H*v**2/(2*k*T))
	K2 = 4*pi*v**2
	return K*K1*K2